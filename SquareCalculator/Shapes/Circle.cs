﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SquareCalculator.Shapes
{
    public class Circle : ShapeBase
    {
        private double _r { get; set; }

        public Circle(double r)
        {
            if (r <= 0) throw new ArgumentException("радиус не может быть меньше нуля");
            _r = r;
        }

        public override double GetSquare()
        {
            return Math.PI * Math.Pow(_r, 2);
        }
    }
}
