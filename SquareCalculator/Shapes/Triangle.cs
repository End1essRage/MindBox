﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SquareCalculator.Shapes
{
    public class Triangle : ShapeBase
    {
        private double _a { get; set; }
        private double _b { get; set; }
        private double _c { get; set; }
        public bool IsRectangular { get; set; } = false;

        public Triangle(double a, double b, double c) 
        {
            _a = a;
            _b = b;
            _c = c;
            if(!Validate())
                throw new ArgumentException("треугольник с такими сторонами не существует");

            IsRectangular = CheckIsRectangular();
        }

        private bool CheckIsRectangular()
        {
            var array = new double[3] { _a, _b, _c };

            double squareSum = 0;
            double max = 0;

            for(int i = 0; i < 3; i++)
            {
                squareSum += array[i] * array[i];
                if (array[i] > max)
                {
                    max = array[i];
                }
            }

            if(squareSum / 2 == max * max)
                return true;

            return false;
        }

        private bool Validate()
        {
            if(_a <= 0 || _b <= 0 || _c <= 0)   return false;
            if(_a + _b > _c && _a + _c > _b && _b + _c > _a) return true;
            return false;
        }

        public override double GetSquare()
        {
            var halfPerimeter = (_a + _b + _c) / 2.0;

            return Math.Sqrt(
                halfPerimeter
                * (halfPerimeter - _a)
                * (halfPerimeter - _b)
                * (halfPerimeter - _c));
        }
    }
}
