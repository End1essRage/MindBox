﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SquareCalculator.Shapes
{
    public abstract class ShapeBase
    {
        public abstract double GetSquare();
    }
}
