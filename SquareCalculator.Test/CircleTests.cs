﻿using SquareCalculator.Shapes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SquareCalculator.Test
{
    public class CircleTests
    {
        [Fact]
        public void CreatingCircle_ThrowsArgumentException_WhenRadiusZeroOrNegative()
        {
            Assert.Throws<ArgumentException>(() => { ShapeBase circle = new Circle(0); });
            Assert.Throws<ArgumentException>(() => { ShapeBase circle = new Circle(-1); });
        }

        [Fact]
        public void GetSquare_ReturnsCorrectSquare()
        {
            double radius = 4;
            ShapeBase circle = new Circle(radius);

            var expected = Math.PI * Math.Pow(radius, 2);
            var actual = circle.GetSquare();

            Assert.Equal(expected, actual);
        }
    }
}
