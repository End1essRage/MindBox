﻿using SquareCalculator.Shapes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SquareCalculator.Test
{
    public class TriangleTests
    { 
        [Fact]
        public void CreatingTriangle_ThrowsArgumentException_WhenIncorrectSides()
        {
            Assert.Throws<ArgumentException>(() => { ShapeBase triangle = new Triangle(1, 1, 9); });     
        }

        [Fact]
        public void CreatingTriangle_ThrowsArgumentException_WhenSideZeroOrNegative()
        {  
            Assert.Throws<ArgumentException>(() => { ShapeBase triangle = new Triangle(0, 5, 7); });
            Assert.Throws<ArgumentException>(() => { ShapeBase triangle = new Triangle(-1, 5, 7); });
        }

        [Fact]
        public void IsRectangular_ReturnsTrue_WhenRectangular()
        {
            ShapeBase triangle = new Triangle(3, 5, 4);

            Assert.True((triangle as Triangle).IsRectangular);
        }

        [Fact]
        public void IsRectangular_ReturnsFalse_WhenNotRectangular()
        {
            ShapeBase triangle = new Triangle(3, 6, 4);

            Assert.False((triangle as Triangle).IsRectangular);
        }

        [Fact]
        public void GetSquare_ReturnsCorrectSquare()
        {
            ShapeBase triangle = new Triangle(3, 5, 4);

            Assert.Equal(triangle.GetSquare(), 6);
        }

    }
}
